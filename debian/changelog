mutt-wizard (3.3.1-4) unstable; urgency=medium

  * debian/patches/020_fix-prefix-and-bash-term-in-posix.patch: updated to fix
    BUG RC. (Closes: #1031402)
  * debian/patches/050_fix-smtp_authenticators.patch: created to fix
    smtp_authenticators in mutt-wizard.muttrc.

 -- Braulio Henrique Marques Souto <braulio@disroot.org>  Sat, 16 Dec 2023 15:20:47 -0300

mutt-wizard (3.3.1-3) unstable; urgency=medium

  [ Jonathan Dowland ]
  * debian/control: added 'procps' in Depends field to providing /usr/bin/pgrep.
    (Closes: #1043364)

 -- Braulio Henrique Marques Souto <braulio@disroot.org>  Sat, 16 Dec 2023 12:54:37 -0300

mutt-wizard (3.3.1-2) unstable; urgency=medium

  * Upload to unstable.
  * debian/control:
      - Added Josenilson Ferreira da Silva in Uploaders.
      - Bumped Standards-Version to 4.6.2.
  * debian/copyright: updated packaging copyright years.
  * debian/patches/040_change-name-mailsync.patch: created to change the binary
    name from mailsync to mw-mailsync in upstream files.
  * debian/rules: added target 'execute_after_dh_install' to fix conflict.
    Thanks to Andreas Beckmann <anbe@debian.org>. (Closes: #1029021)
  * debian/tests/control:
      - Added new test command4.
      - Change name from mailsync to mw-mailsync in command3.

 -- Braulio Henrique Marques Souto <braulio@disroot.org>  Thu, 19 Jan 2023 11:19:10 -0300

mutt-wizard (3.3.1-1) experimental; urgency=medium

  [ Braulio Henrique Marques Souto ]
  * Initial release. (Closes: #1009761)

  [ Josenilson Ferreira da Silva ]
  * debian/control: Some paragraphs of the long description.
  * debian/source/format
  * debian/upstream/*

 -- Braulio Henrique Marques Souto <braulio@disroot.org>  Fri, 16 Dec 2022 02:04:12 -0300
