NAME
  openfile - open a file with xdg-open in a external program

SYNOPSIS
  openfile file

DESCRIPTION
  openfile helps open a file with xdg-open from mutt/neomutt in a external
  program without weird side effects.

  It aims to be as simple and functional as possible for the user to associate
  a file extension with the program that will open it.

  It is not necessary to configure a specific program for each extension in
  mutt/neomutt, because this association will come from your environment settings,
  controlled by xdg-open.

  This program ends up not being exclusive to mutt/neomutt, it can be used by
  command line in a terminal by the user to open any file in its respective
  default program, just by using the command as described in the Synopsis field.

SEE ALSO
  mw(1), xdg-open(1)

AUTHOR
  openfile was written by Luke Smith <luke@lukesmith.xyz>.

  This manual page was written by Braulio Henrique Marques Souto <braulio@disroot.org>
  for the Debian project (but may be used by others).
